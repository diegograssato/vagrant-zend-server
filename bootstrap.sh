#!/bin/bash
PHP="5.6"

# Define diretivas que permitirão instalar MySQL sem perguntar senha
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'


#Configura ZendServerRepo
sudo echo "deb http://repos.zend.com/zend-server/8.0/deb_ssl1.0 server non-free" | sudo tee /etc/apt/sources.list.d/zend.list
sudo wget http://repos.zend.com/zend.key -O- 2> /dev/null | sudo apt-key add -

#Configura NginxRepo
sudo echo "deb http://nginx.org/packages/debian/ wheezy nginx" | sudo tee /etc/apt/sources.list.d/nginx.list
sudo wget http://nginx.org/keys/nginx_signing.key -O- 2> /dev/null | sudo apt-key add -

# Atualiza lista de pacotes
sudo apt-get update 1> /dev/null

# Timezone do sistema
sudo cp -p /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
echo "America/Sao_Paulo" | sudo tee /etc/timezone

# Locale do sistema
sudo update-locale LANG=en_US.UTF-8 UTF-8  pt_BR ISO-8859-1 pt_BR.UTF-8 UTF-8
sudo locale -a
sudo locale-gen


echo "Instalando Pacotes necessarios"
sudo apt-get install -y \
zend-server-nginx-php-$PHP \
php-$PHP-xdebug-zend-server \
php-$PHP-dev-zend-server \
vim curl wget git-core htop \
make build-essential g++ curl \
python-software-properties \
mysql-server mongodb-clients mongodb-server \
xclip \
1> /dev/null


# MySQL Config
# Torna o mysql acessivel a partir de qualquer lugar
sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf
mysql --password=root -u root --execute="GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES;"
service mysql restart

# MongoDB Config
# Torna o mongodb acessivel a partir de qualquer lugar
sudo sed -i "s/bind_ip = .*/#bind_ip = 127.0.0.1/" /etc/mongodb.conf
sudo service mongodb restart

if [ "$1" = "java" ]; then
   sudo apt-get install php-$PHP-java-bridge-zend-server -y
fi

sudo echo 'export PATH=$PATH:/usr/local/zend/bin' >> $HOME/.bashrc
sudo echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/zend/lib' >> $HOME/.bashrc
sudo echo 'export PATH=$PATH:/usr/local/zend/bin' >> /etc/profile.d/zend-server.sh
sudo echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/zend/lib' >> /etc/profile.d/zend-server.sh
source $HOME/.bashrc


# PHP Config
PHP_INI="/usr/local/zend/etc/php.ini"
if [ -f "$PHP_INI" ]; then

    echo "Configurando PHP ${PHP}"
    # Exiba todos os erros
    sudo sed -i "s/error_reporting=.*/error_reporting=E_ALL/" $PHP_INI
    sudo sed -i "s/display_errors=.*/display_errors=On/" $PHP_INI

    # Aumenta a quantidade limite de memória
    sudo sed -i "s/memory_limit=.*/memory_limit=-1/" $PHP_INI

    # Aumenta o tempo máximo de execução de cada script
    sudo sed -i "s/max_execution_time=.*/max_execution_time=120/" $PHP_INI

    # tempo que o servidor guarda os dados da sessão antes de enviar para o Garbage Collection
    sudo sed -i "s/session.cookie_lifetime=.*/session.cookie_lifetime=172800/" $PHP_INI # 2 dias em segundos

    # tempo de expiração do cookie PHPSSESIONID
    sudo sed -i "s/gc_maxlifetime=.*/gc_maxlifetime=172800/" $PHP_INI # 2 dias em segundos

    # TIMEZONE == O -r a baixo é pro sed aceitar ?: http://stackoverflow.com/questions/6156259/sed-expression-dont-allow-optional-grouped-string
    sudo sed -i '$ a\date.timezone=America/Sao_Paulo' $PHP_INI


    # Habilita short open tags (<? ?>)
    sudo sed -i "s/short_open_tag=.*/short_open_tag=On/" $PHP_INI

    # Aumenta o tamanho máximo dos uploads para 100MB
    sudo sed -i "s/post_max_size=.*/post_max_size=100M/" $PHP_INI # 2 dias em segundos
    sudo sed -i "s/upload_max_filesize=.*/upload_max_filesize=100M/" $PHP_INI # 2 dias em segundos

    IP=`ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'`

    # Opcache Config
cat << EOF | sudo tee -a /usr/local/zend/etc/conf.d/opcache.ini
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=4000
opcache.revalidate_freq=60
opcache.fast_shutdown=1
opcache.enable_cli=1
EOF

    # XDebug Config
cat << EOF | sudo tee -a /usr/local/zend/etc/conf.d/xdebug.ini
xdebug.scream=0
xdebug.cli_color=1
xdebug.show_local_vars=1
debug.remote_host=${IP}
xdebug.max_nesting_level=250
xdebug.remote_port=9000
xdebug.max_nesting_level=200
xdebug.remote_enable=1
xdebug.idekey="PHPSTORM"
xdebug.remote_autostart = 1
EOF
fi
# Composer
sudo curl -sS https://getcomposer.org/installer | /usr/local/zend/bin/php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod o+r /var/log/nginx/{access,error}.log

echo -e "Atualizando distro"
sudo apt-get upgrade -y 1> /dev/null

echo -e "Limpando sistema"
sudo apt-get autoremove -y 1> /dev/null
sudo apt-get autoclean -y 1> /dev/null
sudo apt-get clean -y 1> /dev/null

echo -e "Instalação completa"
echo -e "
***********************************************************
* Zend Server $PHP was successfully installed.            *
*                                                         *
* To access the Zend Server UI open your browser at:      *
* https://<hostname>:10082/ZendServer (secure)            *
*       or                                                *
* http://<hostname>:10081/ZendServer                      *
***********************************************************
"