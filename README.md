Vagrant + ZendServer
==========================


## Vagrantfile configurado corretamente para levantar ambiente de desenvolvimento com os seguintes softwares:
- Debian 7.8 64 bits
- ZendServer 8.0.1
- PHP 5.6
- MySQL
- Git
- MongoDB
- Servidor Web Nginx

## Levantando o ambiente

```bash
    cp Vagrantfile.dist Vagrantfile
    ## Se necessario  alteração de redirecionamento ou IP altere o arquivo Vagrantfile antes de rodar o comando 'vagrant'.

    vagrant up

```

## Após a instalação para dar continuidade a configuração, acesse:

```bash
    https://<hostname>:10082/ZendServer (secure)

    ou

    http://<hostname>:10081/ZendServer
```

## Se for necessario ativar algum modulo em modo texto siga as instruções abaixo:


- Acesse o painel administrativo
- Vá em:  Administration => Web API
- Adicione os valores nas variaveis

```bash
cat << EOF

sudo echo 'export KEY_NAME=admin' >> $HOME/.bashrc ## Deve ser alterado segundo o que aparecerá na interface
sudo echo 'export KEY_USER_NAME=admin' >> $HOME/.bashrc ## Deve ser alterado segundo o que aparecerá na interface
sudo echo 'export KEY_HASH=2279504259350435551b7777f1c59cfccb74ddb30a95bba8a5985a1dace3b17a' >> $HOME/.bashrc ## Deve ser alterado segundo o que aparecerá na interface
source $HOME/.bashrc

cat << EOF | sudo tee -a php-modulos.sh
#!/bin/bash
#
# Script que habilita algumas extensões do PHP
#
EXTENSOES="mongo redis imagick memcache memcached"

    for e in \${EXTENSOES}; do

        /usr/local/zend/bin/zs-manage extension-on -e \${e} -N \${KEY_NAME} -K \${KEY_HASH} 1> /dev/null
        echo "[ OK ] - Enabled [ \${e} ]"
    done

    /usr/local/zend/bin/zs-manage restart -N \${KEY_NAME} -K \${KEY_HASH}
EOF
sudo chmod +x php-modulos.sh   ## dê permissão de execução
./php-modulos.sh  ## rode o script


```


